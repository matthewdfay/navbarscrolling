//
//  ViewController.m
//  NavBarHideTest
//
//  Created by Matt Fay on 4/12/16.
//  Copyright © 2016 Matt Fay. All rights reserved.
//

#import "ViewController.h"
#import "OUSmoothNavBarHider.h"

@interface ViewController () <UITableViewDataSource, UITableViewDelegate>
@property (nonatomic, assign)BOOL hasAddedConstraints;
@property (nonatomic, strong) UITableView *tableView;
@property (nonatomic, weak) UIViewController <OUSmoothNavBarScrollingDelegate> *smoothNavBarController;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self createTableView];
}

#pragma mark - Creation

- (void)createTableView {
    self.tableView = [UITableView new];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    self.tableView.translatesAutoresizingMaskIntoConstraints = NO;
    self.tableView.backgroundColor = [UIColor cyanColor];
    [self.view addSubview:self.tableView];
    [self.tableView registerClass:[UITableViewCell class] forCellReuseIdentifier:@"cellTest"];
}

#pragma mark - Constraints

- (void)updateViewConstraints {
    if (!self.hasAddedConstraints) {
        [self addConstraints];
        self.hasAddedConstraints = YES;
    }
    
    [super updateViewConstraints];
}

- (void)addConstraints {
    [self addConstraintsForTableView];
}

- (void)addConstraintsForTableView {
    [self.view addConstraints:[self constraintsToHorizontallyAlignTableView]];
    [self.view addConstraints:[self constraintsToVerticallyAlignTableView]];
}

- (NSDictionary *)viewsForLayoutConstraints {
    return @{@"tableView" : self.tableView};
}

- (NSArray *)constraintsToHorizontallyAlignTableView {
    return [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[tableView]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]];
}

- (NSArray *)constraintsToVerticallyAlignTableView {
    return [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[tableView]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]];
}

#pragma mark - TableView

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 200;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellTest"];
    cell.backgroundColor = (indexPath.row %2 ? [UIColor orangeColor] : [UIColor magentaColor]);
    return cell;
}

#pragma mark - ScrollView

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    self.smoothNavBarController = [OUSmoothNavBarHider smoothNavBarScrollingDelegateInControllerHeirarchyFromController:self];
    if (self.smoothNavBarController) {
        CGFloat newYOffset = [self.smoothNavBarController willBeginScrollingFromOffset:scrollView.contentOffset.y];
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, newYOffset)];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.smoothNavBarController) {
        CGFloat newYOffset = [self.smoothNavBarController didScrollToOffset:scrollView.contentOffset.y];
        [scrollView setContentOffset:CGPointMake(scrollView.contentOffset.x, newYOffset)];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate {
    if (!decelerate) {
        if (self.smoothNavBarController) {
            [self.smoothNavBarController didEndScrollingAtOffset:scrollView.contentOffset.y];
        }
    }
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    if (self.smoothNavBarController) {
        [self.smoothNavBarController didEndScrollingAtOffset:scrollView.contentOffset.y];
    }
}

@end
