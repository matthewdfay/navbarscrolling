//
//  OURootViewController.m
//  NavBarHideTest
//
//  Created by Matt Fay on 4/12/16.
//  Copyright © 2016 Matt Fay. All rights reserved.
//

#import "OURootViewController.h"
#import "ViewController.h"
#import "OUSmoothNavBarHidingViewController.h"

@interface OURootViewController ()
@property(nonatomic, assign) BOOL hasAddedConstraints;
@property(nonatomic, strong) UINavigationController *navController;
@property(nonatomic, strong) OUSmoothNavBarHidingViewController *scrollingViewController;
@property(nonatomic, strong) ViewController *initialViewController;
@end


@implementation OURootViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadInitialViewController];
}

#pragma mark - Constraints

- (void)updateViewConstraints {
    if (!self.hasAddedConstraints && self.isViewLoaded) {
        [self addConstraints];
        self.hasAddedConstraints = YES;
    }
    
    [super updateViewConstraints];
}

- (void)addConstraints {
    [self addConstraintsForNavigationController];
}

- (void)addConstraintsForNavigationController {
    [self.view addConstraints:[self constraintsToHorizontallyAlignNavigationControllerView]];
    [self.view addConstraints:[self constraintsToVerticallyNavigationControllerView]];
}

- (NSDictionary *)viewsForLayoutConstraints {
    return @{@"navController" : self.navController.view};
}

- (NSArray *)constraintsToHorizontallyAlignNavigationControllerView {
    return [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[navController]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]];
}

- (NSArray *)constraintsToVerticallyNavigationControllerView {
    return [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[navController]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]];
}

#pragma mark - Initial VC

- (void)loadInitialViewController {
    self.initialViewController = [ViewController new];
    self.initialViewController.view.backgroundColor = [UIColor redColor];
    
    self.scrollingViewController = [OUSmoothNavBarHidingViewController instance];
    self.scrollingViewController.contentViewController = self.initialViewController;
    self.scrollingViewController.view.backgroundColor = [UIColor yellowColor];
    
    self.navController = [[UINavigationController alloc] initWithRootViewController:self.scrollingViewController];
    self.navController.view.backgroundColor = [UIColor blueColor];
    self.navController.view.translatesAutoresizingMaskIntoConstraints = NO;
    self.navController.navigationBar.translucent = NO;
    
    [self.view addSubview:self.navController.view];
    [self addChildViewController:self.navController];
    [self.navController didMoveToParentViewController:self];
}

@end
