//
//  OUSmoothNavBarHidingViewController.h
//  NavBarHideTest
//
//  Created by Matt Fay on 4/13/16.
//  Copyright © 2016 Matt Fay. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OUSmoothNavBarHider.h"

@interface OUSmoothNavBarHidingViewController : UIViewController <OUSmoothNavBarScrollingDelegate>

+ (instancetype)instance;

@property (nonatomic, strong) UIViewController *contentViewController;

@end
