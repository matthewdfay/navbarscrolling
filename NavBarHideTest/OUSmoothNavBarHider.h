//
//  OUSmoothNavBarHider.h
//  NavBarHideTest
//
//  Created by Matt Fay on 4/14/16.
//  Copyright © 2016 Matt Fay. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol OUSmoothNavBarScrollingDelegate <NSObject>

@required
/**
 Returns what the scrollView's offset should be.
 */
- (CGFloat)willBeginScrollingFromOffset:(CGFloat)yOffset;
- (CGFloat)didScrollToOffset:(CGFloat)yOffset;
- (void)didEndScrollingAtOffset:(CGFloat)yOffset;

@end


@interface OUSmoothNavBarHider : NSObject

+ (UIViewController <OUSmoothNavBarScrollingDelegate> *)smoothNavBarScrollingDelegateInControllerHeirarchyFromController:(UIViewController *)controller;

@end
