//
//  OUSmoothNavBarHidingViewController.m
//  NavBarHideTest
//
//  Created by Matt Fay on 4/13/16.
//  Copyright © 2016 Matt Fay. All rights reserved.
//

#import "OUSmoothNavBarHidingViewController.h"

static CGFloat const OUVerticalCutoffHeight = -22.0f;
static CGFloat const OUNavigationBarHeight = 44.0f;

@interface OUSmoothNavBarHidingViewController()
@property (nonatomic, assign)BOOL hasAddedConstraints;
@property (nonatomic, strong)NSLayoutConstraint *navBarImageViewTopConstraint;
@property (nonatomic, strong)UIImageView *navBarImageView;
@property (nonatomic, strong)UIView *contentView;
@property (nonatomic, assign) CGFloat currentOffset;
@property (nonatomic, assign) BOOL navBarDisplayed;
@property (nonatomic, assign) BOOL scrolling;
@end

@implementation OUSmoothNavBarHidingViewController

- (void)setContentViewController:(UIViewController *)contentViewController {
    if (_contentViewController != contentViewController) {
        [_contentViewController.view removeFromSuperview];
        [_contentViewController removeFromParentViewController];
        _contentViewController = contentViewController;
        _contentViewController.view.translatesAutoresizingMaskIntoConstraints = NO;
        [self.contentView addSubview:_contentViewController.view];
        [self addChildViewController:_contentViewController];
        [_contentViewController didMoveToParentViewController:self];
    }
}

//TESTING
+ (instancetype)instance {
    return [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"smoothHiding"];
}

#pragma mark - View Cycle

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    //TESTING
    self.navigationItem.title = @"TEST";
    
    [self displayNavigationBarAndHideNavigationBarImageView];
    [self createNavigationBarImage];
}

#pragma mark - Creation

- (void)awakeFromNib {
    [self createNavBarImageView];
    [self createContentView];
    self.navBarDisplayed = YES;
}

- (void)createNavBarImageView {
    self.navBarImageView = [UIImageView new];
    self.navBarImageView.backgroundColor = [UIColor grayColor];
    self.navBarImageView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.navBarImageView];
}

- (void)createContentView {
    self.contentView = [UIView new];
    self.contentView.backgroundColor = [UIColor brownColor];
    self.contentView.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:self.contentView];
}

#pragma mark - Constraints

- (void)updateViewConstraints {
    if (!self.hasAddedConstraints) {
        [self addConstraints];
        self.hasAddedConstraints = YES;
    }
    
    [super updateViewConstraints];
}

- (void)addConstraints {
    [self addConstraintsForScrollingNavigationController];
    if (self.contentViewController) {
        [self addConstraintsForContentViewController];
    }
}

- (void)addConstraintsForScrollingNavigationController {
    [self.view addConstraints:[self constraintsToHorizontallyAlignScrollingNavigationControllerView]];
    [self.view addConstraints:[self constraintsToVerticallyScrollingNavigationControllerView]];
}

- (void)addConstraintsForContentViewController {
    [self.contentView addConstraints:[self constraintsToHorizontallyAlignContentViewControllerView]];
    [self.contentView addConstraints:[self constraintsToVerticallyAlignContentViewControllerView]];
}

- (NSDictionary *)viewsForLayoutConstraints {
    NSMutableDictionary *views = [NSMutableDictionary new];
    [views addEntriesFromDictionary:@{
                                     @"imageView"   : self.navBarImageView,
                                     @"content"     : self.contentView
                                     }];
    if (self.contentViewController) {
        [views setObject:self.contentViewController.view forKey:@"contentViewControllerView"];
    }
    
    return views;
}

- (NSArray *)constraintsToHorizontallyAlignScrollingNavigationControllerView {
    NSMutableArray *horizontalConstraints = [NSMutableArray new];
    [horizontalConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[imageView]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]]];
    [horizontalConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[content]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]]];
    return [horizontalConstraints copy];
}

- (NSArray *)constraintsToVerticallyScrollingNavigationControllerView {
    NSMutableArray *verticalConstraints = [NSMutableArray new];
    self.navBarImageViewTopConstraint = [NSLayoutConstraint constraintWithItem:self.navBarImageView
                                                                     attribute:NSLayoutAttributeTop
                                                                     relatedBy:NSLayoutRelationEqual
                                                                        toItem:self.view
                                                                     attribute:NSLayoutAttributeTop
                                                                    multiplier:1.0f
                                                                      constant:-OUNavigationBarHeight];
    [verticalConstraints addObject:self.navBarImageViewTopConstraint];
    
    [verticalConstraints addObjectsFromArray:[NSLayoutConstraint constraintsWithVisualFormat:@"V:[imageView(44)][content]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]]];
    
    return [verticalConstraints copy];
}

- (NSArray *)constraintsToHorizontallyAlignContentViewControllerView {
    return [NSLayoutConstraint constraintsWithVisualFormat:@"H:|[contentViewControllerView]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]];
}

- (NSArray *)constraintsToVerticallyAlignContentViewControllerView {
    return [NSLayoutConstraint constraintsWithVisualFormat:@"V:|[contentViewControllerView]|" options:0 metrics:nil views:[self viewsForLayoutConstraints]];
}

#pragma mark - OUSmoothNavBarScrollingDelegate

- (CGFloat)willBeginScrollingFromOffset:(CGFloat)yOffset {
    if (self.navBarDisplayed) {
        [self hideNavigationBarAndDisplayNavigationBarImageView];
    }
    self.currentOffset = yOffset;
    self.scrolling = YES;
    return self.currentOffset;
}

- (CGFloat)didScrollToOffset:(CGFloat)yOffset {
    NSInteger changeInY = 0.0f;
    if (yOffset > 0.0f) {
        changeInY = yOffset - self.currentOffset;
        self.navBarImageViewTopConstraint.constant = (self.navBarImageViewTopConstraint.constant - changeInY);
    } else if (self.navBarImageViewTopConstraint.constant < 0.0f) {
        changeInY = yOffset + self.currentOffset;
        self.navBarImageViewTopConstraint.constant = (self.navBarImageViewTopConstraint.constant - changeInY);
    }
    
    if (self.navBarImageViewTopConstraint.constant < -OUNavigationBarHeight) {
        self.navBarImageViewTopConstraint.constant = -OUNavigationBarHeight;
    } else if (self.navBarImageViewTopConstraint.constant > 0.0f) {
        self.navBarImageViewTopConstraint.constant = 0.0f;
    }
    
    [self.view layoutIfNeeded];
    self.currentOffset = [self contentOffsetForGivenYOffset:yOffset changeinOffset:changeInY];
    return self.currentOffset;
}

- (void)didEndScrollingAtOffset:(CGFloat)yOffset {
    if (self.scrolling) {
        self.scrolling = NO;
        if (self.navBarImageViewTopConstraint.constant < OUVerticalCutoffHeight) {
            [self hideNavigationBarImageViewAnimated];
        } else {
            [self displayNavigationBarImageViewAnimated];
        }
    }
}

- (CGFloat)contentOffsetForGivenYOffset:(CGFloat)yOffset changeinOffset:(CGFloat)changeInOffset {
    CGFloat newYOffset = yOffset;
    
    if (changeInOffset > 0.0f) {
        if (self.navBarImageViewTopConstraint.constant != -OUNavigationBarHeight &&
            self.navBarImageViewTopConstraint.constant <= 0.0f) {
            newYOffset = yOffset - changeInOffset;
        }
    } else if (changeInOffset < 0.0f) {
        if (self.navBarImageViewTopConstraint.constant != 0.0f &&
            self.navBarImageViewTopConstraint.constant >= -OUNavigationBarHeight) {
            newYOffset = yOffset - changeInOffset;
        }
    }

    return newYOffset;
}

#pragma mark - Display/Hide nav bar

- (void)hideNavigationBarAndDisplayNavigationBarImageView {
    [self.navigationController setNavigationBarHidden:YES];
    self.navBarImageViewTopConstraint.constant = 0.0f;
    [self.view layoutIfNeeded];
    self.navBarDisplayed = NO;
}

- (void)displayNavigationBarAndHideNavigationBarImageView {
    [self.navigationController setNavigationBarHidden:NO];
    self.navBarImageViewTopConstraint.constant = -OUNavigationBarHeight;
    [self.view layoutIfNeeded];
    self.navBarDisplayed = YES;
}

- (void)hideNavigationBarImageViewAnimated {
    [self.view layoutIfNeeded];
    [self.navigationController setNavigationBarHidden:YES];
    self.navBarImageViewTopConstraint.constant = -OUNavigationBarHeight;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        self.navBarDisplayed = NO;
    }];
}

- (void)displayNavigationBarImageViewAnimated {
    [self.view layoutIfNeeded];
    self.navBarImageViewTopConstraint.constant = 0.0f;
    [UIView animateWithDuration:0.2 animations:^{
        [self.view layoutIfNeeded];
    } completion:^(BOOL finished) {
        [self.navigationController setNavigationBarHidden:NO];
        self.navBarImageViewTopConstraint.constant = -OUNavigationBarHeight;
        [self.view layoutIfNeeded];
        self.navBarDisplayed = YES;
    }];
}

#pragma mark - Create Image

- (void)createNavigationBarImage {
    UIWindow *keyWindow = [[UIApplication sharedApplication] keyWindow];
    UIGraphicsBeginImageContextWithOptions(self.navigationController.navigationBar.bounds.size, NO, [UIScreen mainScreen].scale);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [keyWindow.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    self.navBarImageView.image = img;
}

@end
