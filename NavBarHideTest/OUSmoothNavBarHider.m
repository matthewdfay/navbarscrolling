//
//  OUSmoothNavBarHider.m
//  NavBarHideTest
//
//  Created by Matt Fay on 4/14/16.
//  Copyright © 2016 Matt Fay. All rights reserved.
//

#import "OUSmoothNavBarHider.h"

@implementation OUSmoothNavBarHider

+ (UIViewController <OUSmoothNavBarScrollingDelegate> *)smoothNavBarScrollingDelegateInControllerHeirarchyFromController:(UIViewController *)controller {
    UIViewController <OUSmoothNavBarScrollingDelegate> *found = nil;
    for (UIViewController <OUSmoothNavBarScrollingDelegate> *vc in controller.navigationController.viewControllers) {
        if ([vc conformsToProtocol:@protocol(OUSmoothNavBarScrollingDelegate)]) {
            found = vc;
            break;
        }
    }
    return found;
}

@end
