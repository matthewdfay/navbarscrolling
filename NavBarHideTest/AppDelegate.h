//
//  AppDelegate.h
//  NavBarHideTest
//
//  Created by Matt Fay on 4/12/16.
//  Copyright © 2016 Matt Fay. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

